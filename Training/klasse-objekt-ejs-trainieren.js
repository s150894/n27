

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
app.set('view engine', 'ejs')
app.use(bodyParser.urlencoded({extended: true}))
app.set('views', 'Training')

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})

// Wenn localhost:3000/klasse-objekt-ejs-trainieren aufgerufen wird ...

app.get('/klasse-objekt-ejs-trainieren',(req, res, next) => {   

    // ... wird klasse-objekt-ejs-trainieren.ejs gerendert:

    res.render('klasse-objekt-ejs-trainieren', {     
        breite : rechteck.breite,    
        laenge : rechteck.laenge,
        geschlecht : schueler.geschlecht,
        alter : schueler.alter,
        vorname : fussballer.vorname,
        nachname : fussballer.nachname,
        verein : fussballer.verein,
        groesse : fussballer.groesse,
        marktwert : fussballer.marktwert
    })
})





// Eine Klasse ist ein Bauplan. Der Bauplan sieht vor wie Objekte erstellt werden.
// Alle die objekt die von einen Baupaln erstellt werden haben die selben Eigenschaften aber möglicherweise unterschiedliche Eigenschaftswerte

// Klassendefinition
// ==================

class Rechteck {
    constructor() {
        this.laenge
        this.breite
    }
}

// Klassen definition für Schüler in einer Schule

class Schueler {
    constructor() {
        this.geschlecht
        this.klasse
        this.alter
        this.vorname
        this.nachname
    }
}

class Fussballer {
    constructor() {
        this.vorname
        this.nachname
        this.verein
        this.groesse
        this.marktwert
    }
}



//Deklaration eines neuen Objektes vom Typ Rechteck
//Deklaration = Bekannt machen

// let rechteck = ...

//instanziierung eines neuen objektes
//instanziierung erkannt man immer am reservierten Wort new
// bei der instanziierung wird Arbeitsspeicher bereitgestellt 
//... = new Rechteck()

//1.Deklaration 2. instanziierung
let rechteck = new Rechteck()

let schueler = new Schueler()

let fussballer = new Fussballer()

//3. Initialisierung (konkrete Eigenschaftswerte werden zugewiesen)

rechteck.breite = 2
rechteck.laenge = 3

schueler.geschlecht = "W"
schueler.alter = 17

fussballer.vorname = "Jo"
fussballer.nachname = "Stuerznickel"
fussballer.verein =  "Borken"
fussballer.groesse = "1.69"
fussballer.marktwert = "420€"


console.log("länge:" +rechteck.laenge)
console.log("breite:" +rechteck.breite)


