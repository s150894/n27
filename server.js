class Konto{
    constructor(){
        this.Kontonummer
        this.Kontoart
        this.iban
    }
}

//Klassendefinition

class Kunde {
    constructor(){
        this.Vorname
        this.Nachname
        this.Geschlecht
        this.IdKunde
        this.Geburtsdatum
        this.Adresse
        this.Kennwort
    }
}
//Deklaration und Instanziierung

let kunde = new Kunde()

// Initialisierung
kunde.IdKunde = 4711
kunde.Kennwort = "123"
kunde.Vorname = "Hildegard"
kunde.Nachname = "Schmidt"
kunde.Geschlecht = "w"
kunde.Geburtsdatum = "1999-12-31"
kunde.Adresse = "Berlin"


const bankleitzahl = 27000000
const laendererkennung = "DE"
const express = require('express')
const iban = require ('iban')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const app = express()
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(bodyParser.urlencoded({extended: true}))
app.use(cookieParser())

const server = app.listen(process.env.PORT || 3000, () => {
    console.log('Server lauscht auf Port %s', server.address().port)    
})



app.get('/',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)
        res.render('index.ejs', {                              
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})

//Wenn die Seite localhost:3000/impressum aufgerufen wird,....

app.get('/impressum',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)

        //... dann wird impressum.ejs gerendert



        res.render('impressum.ejs', {                              
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})

app.get('/login',(req, res, next) => {         
    res.cookie('istAngemeldetAls', '')       
    res.render('login.ejs', {                    
    })
})

app.post('/',(req, res, next) => {   
    
    // Der Wert des Input mit dem name = "idkunde" wird über den Request zugewiesen an die Konstante idkunde
    
    const idKunde = req.body.idKunde
    const kennwort = req.body.kennwort
        
    console.log(idKunde + "==" + kunde.IdKunde + "&&" + kennwort + "==" + kunde.Kennwort)
    // Wenn der Wert von idkunde dem wert der Eigenschaft kunde.Idkunde entspricht
    //und der wert von kennwort der Eigenschaft kunde.kennwort entspricht, dann werden die anweisungenim Rumpf der 
    // if konrollstruktur abgearbeitet

    if(idKunde == kunde.IdKunde && kennwort == kunde.Kennwort){            
        console.log("Der Cookie wird gesetzt:")
        res.cookie('istAngemeldetAls', idKunde)
        res.render('index.ejs', {           
        })
    }else{            
        console.log("Der Cookie wird gelöscht")
        res.cookie('istAngemeldetAls','')
        res.render('login.ejs', {                    
        })
    }
})

// Wenn die Seite localhost:3000/kontoAnlegen angesurft wird, ...

app.get('/kontoAnlegen',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)

       
     // ...dann wird kontoAnlegen.ejs gerendert


        res.render('kontoAnlegen.ejs', {    
            meldung : ""                          
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})

// wenn der Button auf der Kontoanlegen-Seite gedrückt wird,...

app.post('/kontoAnlegen',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)

        
        let konto = new Konto()
        
        // der wert aus dem input mit den namen 'kontonummer'
        // wird zugewiesen (=) an die eigenschaft Kontonummer 
        //des Objekt namens konto.
        konto.Kontonummer = req.body.kontonummer
        
        konto.Kontoart = req.body.kontoart
        konto.iban = iban.fromBBAN(laendererkennung,bankleitzahl + " " + konto.Kontonummer)
        console.log(konto)

     // ...dann wird kontoanlegen.ejs gerendert
        let bankleitzahl = 12345678

        let errechneteIban = iban.fromBBAN("DE",bankleitzahl + " " + konto.Kontonummer)

    console.log(errechneteIban)

        res.render('kontoAnlegen.ejs', {    
  
            meldung : "Das "+ konto.Kontoart +" mit der IBAN "+ errechneteIban + " wurde erfolgreich angelegt."                          
        })
    }else{
        // Die login.ejs wird gerendet und als response an den browser übergeben
        res.render('login.ejs', {                    
        })    
    }
})

app.get('/Stammdatenpflegen',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)

        res.render('Stammdatenpflegen.ejs', {                              
        })
    }else{
        res.render('login.ejs', {                    
        })    
    }
})


app.post('/Stammdatenpflegen',(req, res, next) => {   

    let idKunde = req.cookies['istAngemeldetAls']
    
    if(idKunde){
        console.log("Kunde ist angemeldet als " + idKunde)
        kunde.Nachname = req.body.nachname
        kunde.Kennwort = req.body.kennwort

        res.render('Stammdatenpflegen.ejs', {      
            meldung : "Stammdaten wurden Aktualisiert"                          
        })
    }else{
    
        res.render('login.ejs', {                    
        })    
    }
})

